import { useEffect, useState } from "react"
import { images } from "../data/images"
import 'bootstrap/dist/css/bootstrap.min.css';
function MemoryGame() {
    const BLANK_CARD = "https://img.icons8.com/emoji/400/000000/black-large-square-emoji.png"
    const [imagesArray, setImagesArray] = useState([])
    const [cardsChosen, setCardsChosen] = useState([])
    const [cardsChosenIds, setCardsChosenIds] = useState([])
    const [points, setPoints] = useState(0)

    const [openCards, setOpenCards] = useState([])

    function createCardBoard() {
        const imagesGenerated = images?.concat(...images)
        const shuffledArray = shuffleArray(imagesGenerated)
        setImagesArray(shuffledArray)
    }

    function flipImage(image, index) {

        if (cardsChosenIds?.length === 1 && cardsChosenIds[0] === index) {
            return
        }

        if (cardsChosen?.length < 2) {

            setCardsChosen(cardsChosen => cardsChosen?.concat(image))
            setCardsChosenIds(cardsChosenIds => cardsChosenIds?.concat(index))

            if (cardsChosen?.length === 1) {
                if (cardsChosen[0] === image) {
                    setPoints(points => points + 2)
                    setOpenCards(openCards => openCards?.concat([cardsChosen[0], image]))
                }
                setTimeout(() => {
                    setCardsChosenIds([])
                    setCardsChosen([])
                }, 700)

            }
        }
    }

    function isCardChosen(image, index) {
        return cardsChosenIds?.includes(index) || openCards?.includes(image)
    }


    function shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        console.log(array)
        return array
    }

    function startOver() {
        setCardsChosenIds([])
        setCardsChosen([])
        setPoints(0)
        setOpenCards([])
    }

    useEffect(() => {
        createCardBoard()
    }, [])

    return (
        <div>
            <h2>Main Game Inget-Ingetan dulu bestie</h2>
            <h3>Points: {points}</h3>
            <button className="App-button" onClick={startOver}>Yuk Mulai</button>
            <div className="row no-gutters">
                {imagesArray?.map((image, index) => {
                    return (
                        <div className="col-4 col-lg-2" key={index} onClick={() => flipImage(image, index)}>
                            <img src={isCardChosen(image, index) ? image : BLANK_CARD} alt="" className={`img-fluid img-fixed`} />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
export default MemoryGame