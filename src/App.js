import { useState } from "react"
import './App.css';
import MemoryGame from "./components/game";

function App() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <br></br>
          <h4>Total Click</h4>
          <h1>{count}</h1>
          <button className="App-button" onClick={() => setCount(count + 1)}>Tambah</button>
          <button className="App-button" onClick={() => setCount(count - 1)}>Kurang</button>
        </div>

        <div>
          <h4>Selamat datang Mr.{name} !!!</h4>
          <button className="App-button" onClick={() => setName("Rizki")}>Rizki</button>
          <button className="App-button" onClick={() => setName("Bambang")}>Bambang</button>
        </div>

        <div className="container my-5">
          <MemoryGame />
        </div>
      </header>
    </div>
  );
}

export default App;
